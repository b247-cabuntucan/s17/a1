/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-show an alert to thank the user for their input.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

function userInfo(){
	let userName = prompt("What's your name?");
	let userAge = prompt("How old are you?");
	let userAddress = prompt("Where do you live?");
	alert("Thank you for providing your information!");


	console.log("Hello, " + userName);
	console.log(userName + " is " + userAge + " years old.");
	console.log(userName + " live in " + userAddress);
}

userInfo();

/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

function printMyFavBands(){
    console.log("The following are my top 5 bands:");
	console.log("1. My Chemical Romance");
	console.log("2. Falling in Reverse");
	console.log("3. Beartooth");
	console.log("4. Motionless in White");
	console.log("5. Suicide Silence");
}

printMyFavBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

function printMyFavMovies(){
    console.log("The following are my top 5 movies:");
	console.log("1. Three Idiots");
	console.log("Rotten Tomatoes Rating: 100%");
	console.log("2. You're Name");
	console.log("Rotten Tomatoes Rating: 98%");
	console.log("3. Avatar");
	console.log("Rotten Tomatoes Rating: 82%");
	console.log("4. Toy Story");
	console.log("Rotten Tomatoes Rating: 100%");
	console.log("5. Lightyear");
	console.log("Rotten Tomatoes Rating: 96%");
}

printMyFavMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

let printFriends = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printFriends();

// console.log(friend1);
// console.log(friend2);
